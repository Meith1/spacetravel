#ifndef __Sphere__
#define __Sphere__

#include "GLSLShader.h"
#include "Vertex.h"
#include "Util.h"
#include "Camera.h"

#include <glm/glm.hpp>

#include <vector>

const int STACKS = 20;
const int SLICES = 20;

class Sphere
{
public:
	Sphere();
	~Sphere();

	void setup(GLSLShader* shader);
	void init(glm::vec3 position, Util* util);
	void update();
	void render(GLSLShader* shader);
	void close();

	glm::vec3 getPosition();

private:

	//Vertex m_pVertices[(STACKS + 1) * (SLICES + 1)];
	std::vector<Vertex> m_pVertices;
	int m_pNumVertices;

	//GLushort m_pIndices[(STACKS * SLICES + SLICES) * 6];
	std::vector<GLushort> m_pIndices;

	int m_pNumIndices;
	GLenum m_pRendererMode;
	GLenum m_pPolygonMode;

	GLuint m_pVaoID;
	GLuint m_pVboID;
	GLuint m_pVioID;

	glm::vec3 m_pTranslationVector;
	glm::vec3 m_pRotationVector;
	glm::vec3 m_pScaleVector;

	glm::mat4 m_pRotationMatrix;
	glm::mat4 m_pTranslationMatrix;
	glm::mat4 m_pScalingMatrix;
	glm::mat4 m_pModelMatrix;

	glm::mat4 m_pMVP;

	float m_pRadius;
	glm::vec3 m_pPosition;
	glm::vec3 m_pRotation;
	glm::vec3 m_pScale;
};

#endif