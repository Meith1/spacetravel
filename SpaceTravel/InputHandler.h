#ifndef __InputHandler__
#define __InputHandler__

#include <SDL/SDL.h>

class InputHandler
{
public:
	
	static InputHandler* Instance()
	{
		if (s_pInstance == 0)
		{
			s_pInstance = new InputHandler();
		}

		return s_pInstance;
	}

	void update();
	bool isKeyDown(SDL_Scancode key);

private:
	InputHandler();
	~InputHandler();

	static InputHandler* s_pInstance;

	const Uint8* m_keystates;

	void onKeyDown();
	void onKeyUp();
};

#endif