#include "Game.h"
#include "Window.h"
#include "Camera.h"
#include "InputHandler.h"

#include <GL/glew.h>
#include <glm/gtc/constants.hpp>

#include <iostream>
#include <cmath>
#include <ctime>

Game* Game::s_pInstance = 0;

Game::Game()
{
	m_pShader = new GLSLShader;
	m_pUtil = new Util;
}

Game::~Game()
{
	delete m_pShader;
}

bool Game::init()
{
	Window::Instance()->createWindow("Space travel", 1024, 768, 0);
	m_pUtil->init();
	//load shader
	m_pShader->loadFromFile(GL_VERTEX_SHADER, "shaders/shader.vert");
	m_pShader->loadFromFile(GL_FRAGMENT_SHADER, "shaders/shader.frag");

	//compile and link shader 
	m_pShader->createAndLinkProgram();
	m_pShader->use();
	{
		//add attributes and uniforms
		m_pShader->addAttribute("vVertex");
		m_pShader->addAttribute("vColor");
		m_pShader->addUniform("MVP");
	}
	m_pShader->unUse();

	glm::vec3 position = glm::vec3(0);

	/*for (int i = 0; i < 1000; i++)
	{
		float x=0, y=0, z=0;
		Sphere* tempSphere = new Sphere();
		x += rand() % 500 - 250;
		y += rand() % 500 - 250;
		z += rand() % 500 - 250;
		position = glm::vec3(x, y, z);
		tempSphere->init(m_pShader, position, m_pUtil);
		m_pSphere.push_back(tempSphere);
	}*/

	Sphere* tempSphere = new Sphere();
	tempSphere->setup(m_pShader);
	tempSphere->init
		
		(position, m_pUtil);
	m_pSphere.push_back(tempSphere);

	for (int i = 0; i < m_pSphere.size(); i++)
	{
		float distance = glm::distance(m_pSphere[i]->getPosition(), Camera::Instance()->getCameraPosition());
		m_pDisplayContainer[distance] = m_pSphere[i];
	}

	m_bRunning = true;
	m_bCamMoved = true;
	return true;
}

void Game::handleEvents()
{
	glm::vec3 veclocity = glm::vec3(1.0f);

	InputHandler::Instance()->update();
	
	if (InputHandler::Instance()->isKeyDown(SDL_SCANCODE_W))
	{
		glm::vec3 displacement = Camera::Instance()->CamLookDirection() * veclocity;
		Camera::Instance()->setCameraPosition(Camera::Instance()->getCameraPosition() + displacement);
		Camera::Instance()->setCameraDirection(Camera::Instance()->getCameraDirection() + displacement);
		m_bCamMoved = true;
	}

	if (InputHandler::Instance()->isKeyDown(SDL_SCANCODE_S))
	{
		glm::vec3 displacement = Camera::Instance()->CamLookDirection() * veclocity;
		Camera::Instance()->setCameraPosition(Camera::Instance()->getCameraPosition() - displacement);
		Camera::Instance()->setCameraDirection(Camera::Instance()->getCameraDirection() - displacement);
		m_bCamMoved = true;
	}

	if (InputHandler::Instance()->isKeyDown(SDL_SCANCODE_D))
	{
		glm::vec3 displacement = (glm::cross(Camera::Instance()->CamLookDirection(), Camera::Instance()->getCameraUpVector()) * veclocity);
		Camera::Instance()->setCameraPosition(Camera::Instance()->getCameraPosition() + displacement);
		Camera::Instance()->setCameraDirection(Camera::Instance()->getCameraDirection() + displacement);
		m_bCamMoved = true;
	}

	if (InputHandler::Instance()->isKeyDown(SDL_SCANCODE_A))
	{
		glm::vec3 displacement = (glm::cross(Camera::Instance()->CamLookDirection(), Camera::Instance()->getCameraUpVector()) * veclocity);
		Camera::Instance()->setCameraPosition(Camera::Instance()->getCameraPosition() - displacement);
		Camera::Instance()->setCameraDirection(Camera::Instance()->getCameraDirection() - displacement);
		m_bCamMoved = true;
	}

	if (InputHandler::Instance()->isKeyDown(SDL_SCANCODE_LEFT))
	{
		glm::vec3 displacement = (glm::cross(Camera::Instance()->CamLookDirection(), Camera::Instance()->getCameraUpVector()) * veclocity);
		Camera::Instance()->setCameraPosition(Camera::Instance()->getCameraPosition() + displacement);
		m_bCamMoved = true;
	}

	if (InputHandler::Instance()->isKeyDown(SDL_SCANCODE_RIGHT))
	{
		glm::vec3 displacement = (glm::cross(Camera::Instance()->CamLookDirection(), Camera::Instance()->getCameraUpVector()) * veclocity);
		Camera::Instance()->setCameraPosition(Camera::Instance()->getCameraPosition() - displacement);
		m_bCamMoved = true;
	}
}

void Game::update()
{
	if (m_bCamMoved)
	{
		m_pDisplayContainer.clear();
		for (int i = 0; i < m_pSphere.size(); i++)
		{
			float distance = glm::distance(m_pSphere[i]->getPosition(), Camera::Instance()->getCameraPosition());
			m_pDisplayContainer[distance] = m_pSphere[i];

			
			m_pSphere[i]->update();

			
		}
		Camera::Instance()->update();
		m_bCamMoved = false;
	}
}

void Game::render()
{
	glClearDepth(1.0);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glEnable(GL_DEPTH_TEST);

	glEnable(GL_CULL_FACE);

	glCullFace(GL_BACK);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	/*for (int i = 0; i < m_pSphere.size(); i++)
	{
		glm::vec3 tempSphere = m_pSphere[i]->getPosition();
		glm::vec3 tempCamera = Camera::Instance()->getCameraPosition();

		tempSphere -= tempCamera;
		if (glm::dot(tempSphere, Camera::Instance()->CamLookDirection()) > 0)
			m_pSphere[i]->render(m_pShader);
	}*/

	
	for (std::map<float, Sphere*>::iterator it = m_pDisplayContainer.begin(); it != m_pDisplayContainer.end(); ++it)
	{
	//	glm::vec3 tempSphere = it->second->getPosition(); 
	//	glm::vec3 tempCamera = Camera::Instance()->getCameraPosition();

		//tempSphere -= tempCamera;

		//if (glm::dot(tempSphere, Camera::Instance()->CamLookDirection()) > 0)
			it->second->render(m_pShader);
	}

	Window::Instance()->swapBuffers();
}

void Game::close()
{
	SDL_DestroyWindow(Window::Instance()->getwindow());
}

void Game::quit()
{
	m_bRunning = false;
	SDL_Quit();
}
