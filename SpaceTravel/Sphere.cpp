#include "Sphere.h"
#include "Camera.h"
#include "Util.h"

#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/euler_angles.hpp>

#include <ctime>
#include <cstdlib>

const unsigned int GalaxySize = 100000;

GLfloat offsetsX[GalaxySize];
GLfloat offsetsY[GalaxySize];
GLfloat offsetsZ[GalaxySize];

Sphere::Sphere()
{

}

Sphere::~Sphere()
{
}

void Sphere::setup(GLSLShader* shader)
{
	m_pNumVertices = (STACKS + 1) * (SLICES + 1);
	m_pNumIndices = (STACKS * SLICES + SLICES) * 6;
	m_pPolygonMode = GL_FILL;
	m_pRendererMode = GL_TRIANGLES;
	m_pRadius = 0.5f;

	for (int i = 0; i <= STACKS; ++i)
	{
		float V = i / (float)STACKS;
		float phi = V * glm::pi <float>();

		// Loop Through Slices
		for (int j = 0; j <= SLICES; ++j)
		{
			float U = j / (float)SLICES;
			float theta = U * (glm::pi <float>() * 2);

			// Calc The Vertex Positions
			float x = cosf(theta) * sinf(phi);
			float y = cosf(phi);
			float z = sinf(theta) * sinf(phi);
			Vertex temp;
			temp.position = glm::vec3(x, y, z)*m_pRadius;
			m_pVertices.push_back(temp);
		}
	}

	for (int i = 0; i < SLICES * STACKS + SLICES; ++i)
	{
		m_pIndices.push_back(i);
		m_pIndices.push_back(i + SLICES + 1);
		m_pIndices.push_back(i + SLICES);

		m_pIndices.push_back(i + SLICES + 1);
		m_pIndices.push_back(i);
		m_pIndices.push_back(i + 1);
	}

	for (int i = 0; i < m_pNumVertices; i += 3)
	{
		m_pVertices[i].color = glm::vec3(0.5, 1, 0);
		m_pVertices[i + 1].color = glm::vec3(0.5, 0.5, 0);
		m_pVertices[i + 2].color = glm::vec3(1, 0, 0);
	}

	//setup mesh vao and vbo stuff
	glGenVertexArrays(1, &m_pVaoID);
	glGenBuffers(1, &m_pVboID);
	glGenBuffers(1, &m_pVioID);
	GLsizei stride = sizeof(Vertex);

	//bind vao and vbo stuff
	glBindVertexArray(m_pVaoID);

	glBindBuffer(GL_ARRAY_BUFFER, m_pVboID);

	//pass mesh vertices to buffer objects
	glBufferData(GL_ARRAY_BUFFER, stride*m_pNumVertices, &m_pVertices[0], GL_STATIC_DRAW);

	//enable vertex attribute array for position
	glEnableVertexAttribArray((*shader)["vVertex"]);
	glVertexAttribPointer((*shader)["vVertex"], 3, GL_FLOAT, GL_FALSE, stride, 0);

	//enable vertex attribute for color
	glEnableVertexAttribArray((*shader)["vColor"]);
	glVertexAttribPointer((*shader)["vColor"], 4, GL_FLOAT, GL_FALSE, stride, (const GLvoid*)offsetof(Vertex, color));

	//pass indices to element array buffer
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_pVioID);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_pNumIndices * sizeof(GLushort), &m_pIndices[0], GL_STATIC_DRAW);

	//glm::vec3 offsets[2] = {glm::vec3(5,5,5), glm::vec3(10,10,10)};
	
	
	for (int i = 0; i < GalaxySize; i++)
	{
		offsetsX[i] = rand() % 2000 - 1000;
	}
	GLuint offsetXBufferID;
	glGenBuffers(1, &offsetXBufferID);
	glBindBuffer(GL_ARRAY_BUFFER, offsetXBufferID);
	glBufferData(GL_ARRAY_BUFFER, sizeof(offsetsX), offsetsX, GL_STATIC_DRAW);
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, 0, 0);
	glVertexAttribDivisor(2, 1);

	
	for (int i = 0; i < GalaxySize; i++)
	{
		offsetsY[i] = rand() % 2000 - 1000;
	}
	GLuint offsetYBufferID;
	glGenBuffers(1, &offsetYBufferID);
	glBindBuffer(GL_ARRAY_BUFFER, offsetYBufferID);
	glBufferData(GL_ARRAY_BUFFER, sizeof(offsetsY), offsetsY, GL_STATIC_DRAW);
	glEnableVertexAttribArray(3);
	glVertexAttribPointer(3, 1, GL_FLOAT, GL_FALSE, 0, 0);
	glVertexAttribDivisor(3, 1);

	
	for (int i = 0; i < GalaxySize; i++)
	{
		offsetsZ[i] = rand() % 2000 - 1000;
	}
	GLuint offsetZBufferID;
	glGenBuffers(1, &offsetZBufferID);
	glBindBuffer(GL_ARRAY_BUFFER, offsetZBufferID);
	glBufferData(GL_ARRAY_BUFFER, sizeof(offsetsZ), offsetsZ, GL_STATIC_DRAW);
	glEnableVertexAttribArray(4);
	glVertexAttribPointer(4, 1, GL_FLOAT, GL_FALSE, 0, 0);
	glVertexAttribDivisor(4, 1);
}

void Sphere::init(glm::vec3 position, Util* util)
{

	//initialise all variables
	//position += glm::vec3(5, 5, 5);

	m_pPosition = position;
	m_pRotation = glm::vec3(0,0,0);
	m_pScale = glm::vec3(1);

	m_pTranslationVector = m_pPosition;
	m_pRotationVector = m_pRotation;
	m_pScaleVector = m_pScale;

	m_pTranslationMatrix = glm::translate(glm::mat4(), m_pTranslationVector);
	m_pRotationMatrix = glm::eulerAngleYXZ(m_pRotationVector.y, m_pRotationVector.x, m_pRotationVector.z);
	m_pScalingMatrix = glm::scale(glm::mat4(), m_pScaleVector);

	m_pModelMatrix = m_pTranslationMatrix * m_pRotationMatrix * m_pScalingMatrix;
}

void Sphere::update()
{
	m_pMVP = Camera::Instance()->getProjectionMatrix() * Camera::Instance()->getViewMatrix() * m_pModelMatrix;
}

void Sphere::render(GLSLShader* shader)
{
	shader->use();
	{
		glBindVertexArray(m_pVaoID);
		glUniformMatrix4fv((*shader)("MVP"), 1, GL_FALSE, &m_pMVP[0][0]);
		{
			//set polygon mode
			glPolygonMode(GL_FRONT_AND_BACK, m_pPolygonMode);
			//draw mesh
		//	glDrawElements(m_pRendererMode, m_pNumIndices, GL_UNSIGNED_SHORT, 0);
			glDrawElementsInstanced(m_pRendererMode, m_pNumIndices, GL_UNSIGNED_SHORT, 0, GalaxySize);
		}
	}
	shader->unUse();
}

void Sphere::close()
{

}

glm::vec3 Sphere::getPosition()
{
	return m_pPosition;
}