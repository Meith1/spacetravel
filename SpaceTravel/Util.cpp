#include "Util.h"

#include <ctime>

Util::Util()
{
	randSeed = 0;
}

Util::~Util()
{
}

void Util::init()
{
	randSeed=time(NULL);
	srand(++randSeed);
}


void Util::updateSeed()
{
	srand(++randSeed);
}

float Util::randValue()
{
	return (1.f / (rand() % 10 + 1));
}
