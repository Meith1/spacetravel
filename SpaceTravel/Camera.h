#ifndef __Camera__
#define __Camera__

#include <glm/glm.hpp>

class Camera
{
public:

	static Camera* Instance()
	{
		if (s_pInstance == 0)
		{
			s_pInstance = new Camera();
			return s_pInstance;
		}

		return s_pInstance;
	}

	glm::mat4 getViewMatrix();
	glm::mat4 getProjectionMatrix();

	void setCameraPosition(glm::vec3 position);
	void setCameraDirection(glm::vec3 direction);
	
	glm::vec3 getCameraPosition();
	glm::vec3 getCameraDirection();
	glm::vec3 getCameraUpVector();

	glm::vec3 CamLookDirection();

	void update();

private:
	static Camera* s_pInstance;

	Camera();
	~Camera();

	glm::mat4 m_pViewMatrix;
	glm::mat4 m_pProjectionMatrix;

	glm::vec3 m_pCameraPosition;
	glm::vec3 m_pCameraDirection;
	glm::vec3 m_pCameraUpVector;
};

#endif 
