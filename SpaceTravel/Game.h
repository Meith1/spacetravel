#ifndef __Game__
#define __Game__

#include "GLSLShader.h"
#include "Sphere.h"

#include <vector>
#include <map>
#include <functional> 

class Game
{
public:

	static Game* Instance()
	{
		if (s_pInstance == 0)
		{
			s_pInstance = new Game();
			return s_pInstance;
		}

		return s_pInstance;
	}

	bool init();
	void handleEvents();
	void update();
	void render();
	void close();
	void quit();

	bool running() { return  m_bRunning; }

private:
	Game();
	~Game();

	static Game* s_pInstance;
	bool m_bRunning;
	bool m_bCamMoved;

	GLSLShader* m_pShader;

	Util* m_pUtil;

	std::vector<Sphere*> m_pSphere;
	std::map<float, Sphere*> m_pDisplayContainer;
};

#endif
