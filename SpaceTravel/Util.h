#ifndef __Util__
#define __Util__
#include <glm/glm.hpp>
#include <vector>


class Util
{
public:
	Util();
	~Util();

	void init();

	void updateSeed();

	float randValue();

	void nonLinearMap(float* x, float* y, float* z);

private:

	int randSeed;


};
#endif