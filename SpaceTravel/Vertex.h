#ifndef __Vertex__
#define __Vertex__

#include <glm/glm.hpp>

struct Vertex 
{
	glm::vec3 position;
	glm::vec3 color;
};

#endif